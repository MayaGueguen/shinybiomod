#' Read raster files in memory
#'
#' Modified from https://github.com/luismurao/nichetoolbox/blob/master/R/rlayers_ntb.R
#' reads raster layers with the same extent and resolution in all `raster` accepted formats
#'
#' @param path to a raster directory
#'
#' @return A 'RasterStack' object
#' @export
#' @examples
#' rlayers_shbmd('~/path to a raster directory')
rlayers_shbmd <- function(layers_path){

  tryCatch({
	# Regular expression to check for rasters layer with different formats
	ras_formats <- "(*.grd$)|(*.asc$)|(*.bil$)|(*.sdat$)|(*.rst$)|(*.tif$)|(*.envi$)|(*.img$)|(*hdr.adf$)"

	if(any(grepl('hdr.adf',list.files(layers_path, pattern = ras_formats, full.names = TRUE, recursive=TRUE))))
		layers_paths <- gtools::mixedsort(list.files(layers_path, pattern = ras_formats,full.names = TRUE, recursive=TRUE)) # mixedsort is a helper function to sort the files paths names correctly (not in alphanumeric order)
	else
		layers_paths <- gtools::mixedsort(list.files(layers_path, pattern = ras_formats,full.names = TRUE))

	# Read raster layers
	if(length(layers_paths)>0)
		layers_list <- lapply(layers_paths, raster::raster)
	else
		return(NULL)
	names(layers_list) <- "x"

	# Check resolution and extent
	check_args <- list(extent=TRUE,rowcol=FALSE,crs=FALSE,res=TRUE,stopiffalse=TRUE,showwarning=TRUE)

	# Check resolution and extent
	resol <- unlist(lapply(layers_list,function(x) {
		ras_extent <- raster::extent(x)
		resol <- paste0(res(x), c(ras_extent[1],ras_extent[2],ras_extent[3],ras_extent[4]),  collapse = "_")
	}))

	# Read layers only if they have the same extent and resolution
	if(length(unique(resol))==1){
		raster_stack <- try(raster::stack(layers_paths),silent=TRUE)
		if(inherits(raster_stack,"error")) return(NULL)
		if(any(grepl('hdr.adf',list.files(layers_path, pattern = ras_formats, full.names = TRUE, recursive=TRUE))))
			names(raster_stack) <- replace(names(raster_stack),
			                               grepl('^hdr',names(raster_stack)),
			                               basename(dirname(layers_paths))[grepl('hdr.adf',list.files(layers_path, pattern = ras_formats, full.names = TRUE, recursive=TRUE))])
		return(raster_stack)
	}
	else{
		warning(paste("Raster layers must have the same extent and resolution !"))
		return(NULL)
	}
  },error=function(err) return(NULL))

}

#' Crop/mask raster with a spatial object while accounting for cell coverage
#'
#'
#' crop then mask a raster object with a spatial shape while making sure that boundary cells in contact with the shape are not masked out
#'
#' @param x, A `Raster*` object
#' @param y, A `Spatial*` object
#' @return A `Raster*` object
#' @export
maskCover <- function(x, y, do.crop=TRUE, ...){

  # check inputs
  stopifnot(inherits(x,c("RasterLayer","RasterStack")))
  stopifnot(inherits(y,c("SpatialPolygons","SpatialPolygonsDataFrame","SpatialLines","SpatialPoints")))

  # first match the extent of the vector shape
  if(do.crop)
    stack_reference <- raster::crop(x, y)
  else
    stack_reference <- x

  # then identify cells that at least partially overlap with the shape
  mask_raster <- raster::rasterize(y, stack_reference[[1]], getCover=TRUE)

  # finally mask out cells not overlapping with the shape
  mask_raster[mask_raster==0] <- NA
  raster::stack(raster::mask(stack_reference,mask=mask_raster,...))
}
