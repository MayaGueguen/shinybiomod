#' Function to search and download worldclim data
#'
#' @description Searchs worldclim data using the function getData from the raster package
#' @param cond Climate condition (\code{"future_cond"} for future climate condition or \code{"current_cond"} for current)
#' @param rcp Representative concentration pathways
#' @param gcm Global circulation model to use.
#' @param year Year of projection in the future (\code{"50"} or \code{"70"})
#' @param download Download raster layers into a stack. Default is TRUE
#' @param res Spatial resolution of raster layers (\code{"2.5"}, \code{"5"} or \code{"10"})
#' @param var Variable to extract (\code{"tmin"}, \code{"tmax"} or \code{"bio"})
#' @param path Path to a directory where the raster layers will be stored. Default is NULL.
#' @return A raster stack from parameters
#' @export
#' @examples
#' # WORLDCLIM search
#' # raster_stack <- searh_worldclim_data(cond='futur_cond', rcp = 45,gcm=100, year=70,	res=10, var='bio',download=FALSE)
#' # plot(raster_stack[[1]])
search_worldclim_rasters = function(cond, rcp, gcm, year, res, var, lon=NULL, lat=NULL, path=NULL, download=TRUE){

    if(cond=="future_cond"){
    if(missing(rcp) || missing(gcm) || missing(year))
      return(NULL)
  }
	if(cond=="current_cond"){
	  if(!missing(res) && res==0.5)
	    raster_stack <- tryCatch(raster::getData(name="worldclim", path=path, var = var, res = res, lon=lon, lat=lat, download = download), error=function(err) return(err))
	  else
		  raster_stack <- tryCatch(raster::getData(name="worldclim", path=path, var = var, res = res, download = download), error=function(err) return(err))
	}
	else if(cond=="future_cond"){
		NBRCP = length(rcp)
		NBGCM = length(gcm)
		raster_stack <- tryCatch({
		              raster::stack(
              		  mapply(FUN=function(rcp,gcm){
              		      raster::getData(name="CMIP5", path=path, var = var, year = year, res = res, rcp = rcp, model = gcm, download = download)
              		    },rcp=rep(rcp,each=NBGCM),gcm=rep(gcm,NBRCP)
              			)
			            )},error=function(err) NULL)
	}else{
		return(NULL)
	}
	return (raster_stack)
}


#' Renaming WorldClim raster layer
#'
#' Function that renames WorldClim raster layer into the form [variable name][number of the layer].
#' i.e. it strips off gcm, rcp and year from layer name
#'
#' @param variable  (\code{"bio"}, \code{"prec"}, \code{"tmin"}, \code{"tmax"} or \code{"tmean"})
#' @param layernames a vector of names retrieves from raster layers object ('RasterLayer', 'RasterStack' or 'RasterBrick')
#' @return Character vector of raster layer names
#' @export
rename_Worldclim_layers <- function(variable,layernames){

	first_char = substr(variable, 1,1)
	second_char = substr(variable,2,2)
	last_char = substr(variable,nchar(variable),nchar(variable))

	if (grepl("bio|prec|tmin|tmax|tmean", variable))
		gsub(sprintf("[a-z][a-z][2-8][0-6][%s][%s|%s][5|7][0]",first_char,second_char,last_char),variable,layernames)
	else
		stop("rename_Worldclim_layers() : Invalid 'variable' argument")
}






