# Correlation Analysis

suggested_explvar_from_corr_analysis <- reactiveValues(suggested_vifcor_var=NULL,suggested_vifstep_var=NULL)

# --------------------------------------------------------

# TRIGGERS

# --------------------------------------------------------
hasComputedCorrelations <-reactiveVal(FALSE)

# --------------------------------------------------------

# OBSERVERS

# --------------------------------------------------------

# updates area for correlation analysis
observe({

	cor_data_from_area <- NULL

	if(!is.null(rasterLayers()) | !is.null(raster_worldclim_current()))
		cor_data_from_area <- c(cor_data_from_area, "All raster area" = "all_area")

	if(!is.null(myRegionRasters()))
		cor_data_from_area <- c("Your region area" = "area_of_interest")

	if(hasSavedBackground())
	  cor_data_from_area <- c(cor_data_from_area, "Your constrained area" = "area_constrained")

	updateSelectizeInput(session, inputId = "cor_data_from_area", choices = cor_data_from_area, selected = input$cor_data_from_area)

})

# updates niche space area (for extraction) in the same time
observeEvent(input$cor_data_from_area,{
	if(!is.null(input$cor_data_from_area)){
		rasters$area <- input$cor_data_from_area
		updateSelectizeInput(session, inputId = "niche_space_area", selected = input$cor_data_from_area)
	}
})

# updates environmental layers available for correlation analysis
observe({

	cor_data_from_env_layers <- NULL

	if(!is.null(rasterLayers()))
		cor_data_from_env_layers <- c(cor_data_from_env_layers, "Your own raster layers" = "user_layers")

	if(!is.null(raster_worldclim_current()))
		cor_data_from_env_layers <- c(cor_data_from_env_layers, "WorldClim raster layers" = "wc_current_layers")

	updateSelectizeInput(session, inputId = "cor_data_from_env_layers", choices = cor_data_from_env_layers, selected = input$cor_data_from_env_layers)
})

# updates niche layers dataset (for extraction) in the same time
observe({

	if(!is.null(input$cor_data_from_env_layers)){
		rasters$origins <- input$cor_data_from_env_layers
		updateSelectizeInput(session, inputId = "niche_layers_from", selected = input$cor_data_from_env_layers)
	}
})

# updates names of available variables for correlation analysis
observe({
	input$cor_data_from_env_layers
	cor_data_from_explvar <- NULL
	if(!is.null(input$cor_data_from_env_layers) && !is.null(myRasterStack()) )
		cor_data_from_explvar <- names(myRasterStack())

	updateSelectizeInput(session, inputId = "cor_data_from_explvar", choices = cor_data_from_explvar, selected = cor_data_from_explvar)

})

# shows/hides correlation analysis selection
observeEvent(input$cor_data_from_explvar, ignoreNULL=TRUE, {
  if(length(input$cor_data_from_explvar)==0L)
    shinyjs::hide('cor_analysis_div')
  else
    shinyjs::show('cor_analysis_div')
})


# updates radioButtons input$cor_analysis
observe({
	input$cor_analysis

	updateRadioButtons(session,"cor_analysis", selected=input$cor_analysis)
})

# updates maximum number of observations for computations
observe({
  if(countNonNACells()>0L)
	  updateNumericInput(session,"cor_maxObs", value=min(input$cor_maxObs,countNonNACells()))
})


# runs correlation analysis using vif correlation method when the user click on run and chose vif correlation method
observeEvent(

	eventExpr={
		input$run_vifcor
	},

	handlerExpr = {

		# When the button is clicked, wrap the code in a call to `withBusyIndicatorServer()`
		withBusyIndicatorServer(buttonId="run_vifcor",

			expr = {

				Sys.sleep(1)
				if(!is.null(input$run_vifcor) && input$run_vifcor>0 && !is.null(vif_cor())){

				  if(hasComputedCorrelations())
				    hasComputedCorrelations(FALSE)

					vifCor = vif_cor()
					suggested_vifcor_var <- names(exclude(subset(myRasterStack(),subset=input$cor_data_from_explvar), vifCor))

					# Update the list of suggested variables for modelling
					#updateSelectizeInput(session,"niche_layers",selected=suggested_vifcor_var)
					#updateRadioButtons(session, inputId='suggest_explvar', selected=suggested_vifcor_var)
					suggested_explvar_from_corr_analysis$suggested_vifcor_var <- suggested_vifcor_var

					# report a summary of correlation analysis
					output$cor_Report <- renderUI({
						if(input$cor_analysis=="VIF_COR")
							suppressWarnings(ShinyBIOMOD::show_report(input$cor_analysis, vifCor, threshold=isolate(input$cor_vifcor_threshold)))
					})

					# table of vif for all variables
					output$cor_all_VIF <- renderTable({
						vif_table()
					},striped=TRUE, width= '50%')

					# table of vif for remained variables after exclusion of collinear ones
					output$cor_rest_VIF <- renderTable({
						vifCor@results
					},striped=TRUE, width= '50%')

					# correlation matrix of remained variables after exclusion of collinear ones
					output$cor_Mat <- DT::renderDT({
						round(vifCor@corMatrix, 4)
					},options = list(lengthMenu = c(5, 10, 25, 50, 100, 500), pageLength = 25))

					hasComputedCorrelations(TRUE)

				}
			}
		)
	}
)

# runs correlation analysis using vif stepwise method when the user click on run and chose stepwise method
observeEvent(

	eventExpr={
		input$run_vifstep
	},

	handlerExpr = {

		withBusyIndicatorServer(buttonId="run_vifstep",

			expr = {

				Sys.sleep(1)
				if(!is.null(input$run_vifstep) && input$run_vifstep>0 && !is.null(vif_step())){

				  if(hasComputedCorrelations())
				    hasComputedCorrelations(FALSE)

					vifStep = vif_step()
					suggested_vifstep_var <-names(exclude(subset(myRasterStack(),subset=input$cor_data_from_explvar), vifStep))

					# Update the list of suggested variables for modelling
					#updateSelectizeInput(session,"niche_layers",selected=suggested_vifstep_var)
					#updateRadioButtons(session, inputId='suggest_explvar', selected=suggested_vifstep_var)
					suggested_explvar_from_corr_analysis$suggested_vifstep_var <- suggested_vifstep_var

					# report a summary of correlation analysis
					output$cor_Report <- renderUI({
						if(input$cor_analysis=="VIF_STEP")
							suppressWarnings(ShinyBIOMOD::show_report(input$cor_analysis, vifStep, threshold=isolate(input$cor_vifstep_threshold), cor_threshold=isolate(input$cor_vifstep_optional_threshold)))
					})

					# table of vif for all variables
					output$cor_all_VIF <- renderTable({
					  vif_table()
					},striped=TRUE, width= '100%')

					# table of vif for remained variables after exclusion of collinear ones
					output$cor_rest_VIF <- renderTable({
					  vifStep@results
					},striped=TRUE, width= '100%')

					# correlation matrix of remained variables after exclusion of collinear ones
					output$cor_Mat <- DT::renderDT({
					  round(vifStep@corMatrix,4)
					},options = list(lengthMenu = c(5, 10, 25, 50, 100, 500), pageLength = 25))

					hasComputedCorrelations(TRUE)


					# # table of vif for all variables
					# output$cor_all_VIF <- renderTable({
					# 	vif_table()
					# },striped=TRUE, width= '100%')
					#
					# # table of vif for remained variables after exclusion of collinear ones
					# output$cor_rest_VIF <- renderTable({
					# 	vifStep@results
					# },striped=TRUE, width= '100%')
					#
					# # correlation matrix of remained variables after exclusion of collinear ones
					# output$cor_Mat <- DT::renderDT({
					# 	vifStep@corMatrix
					# },options = list(lengthMenu = c(5, 10, 25, 50, 100, 500), pageLength = 25))

				}
			}
		)
	}
)


# runs correlation analysis using pairwise correlations method when the user click on run and chose pairwise method
observeEvent(

	eventExpr={
		input$run_pw_cor
	},

	handlerExpr = {

		withBusyIndicatorServer(buttonId="run_pw_cor",

			expr = {

				Sys.sleep(1)
				if(!is.null(input$run_pw_cor) && input$run_pw_cor>0 && !is.null(corr_table())){

					cor_table = corr_table()

					# report a summary of correlation analysis
					output$cor_Report <- renderUI({

						if(input$cor_analysis=="PW_COR")
							ShinyBIOMOD::show_report(input$cor_analysis, cor_table, threshold=isolate(input$cor_pw_threshold))
					})

					# correlation matrix of variables
					output$cor_pw_Mat <- DT::renderDT({
						round(cor_table, 4)
					})
				}
			}
		)
	}
)

# --------------------------------------------------------

# REACTIVE FUNCTIONS

# --------------------------------------------------------

# returns the number of NA cells in raster layers
countNonNACells <- reactive({
	input$cor_analysis
	isolate({
		if(!is.null(input$cor_analysis) && length(input$cor_analysis) > 0L && !is.null(myRasterStack())){
			sum(!suppressWarnings(is.na(myRasterStack()[[1]][])))
		}
		else
			return(0)
	})
})

# compute vif values and returns vif object
vif_table <- reactive({

	input$run_vifcor
	input$run_vifstep

	isolate({
		if(!is.null(myRasterStack()) && !is.null(input$cor_data_from_explvar) && !is.null(input$cor_maxObs)){
			vifObj = vif(subset(myRasterStack(),subset=input$cor_data_from_explvar),maxobservations=if(nchar(input$cor_maxObs)>0L) input$cor_maxObs)
			return(vifObj)
		}
		else
			return(NULL)
	})
})

# performs vif correlation analysis and returns vifcor object
vif_cor <- reactive({

	input$run_vifcor

	isolate({
		if(!is.null(input$run_vifcor) && input$run_vifcor>0 && !is.null(myRasterStack())){

			vifObj = vifcor(subset(myRasterStack(),subset=input$cor_data_from_explvar), th = input$cor_vifcor_threshold, maxobservations=if(nchar(input$cor_maxObs)>0L) input$cor_maxObs)
			return(vifObj)

		}
		else
			return(NULL)
	})
})

# performs vifstep analysis and returns vifstep object
vif_step <- reactive({

	input$run_vifstep

	isolate({
		if(!is.null(input$run_vifstep) && input$run_vifstep>0 && !is.null(myRasterStack())){

			vifObj = vifstep(subset(myRasterStack(),subset=input$cor_data_from_explvar), th = input$cor_vifstep_threshold, maxobservations=if(nchar(input$cor_maxObs)>0L) input$cor_maxObs)
			return(vifObj)

		}
		else
			return(NULL)
	})
})

# computes pearson correlation coefficient of raster layers
# TODO: allow spearman and/or kendall correlation coefficient to be computed
corr_table <- reactive({

	input$run_pw_cor

	isolate({
		if(!is.null(input$run_pw_cor) && input$run_pw_cor>0 && !is.null(myRasterStack())){

			cor_table <- layerStats(subset(myRasterStack(),subset=input$cor_data_from_explvar),stat='pearson',na.rm=TRUE)$"pearson correlation coefficient"

			return(cor_table)
		}
		else
			return(NULL)
	})
})


# --------------------------------------------------------

# OUTPUTS

# --------------------------------------------------------

# returns vif tables
output$vif_tables <- renderUI({

  if(!hasComputedCorrelations())
    return()

  out= tagList(

    shinydashboard::box(width=12,

      tags$hr(style="border: 2px solid !important; color: #fcc201 !important;"),

      h3("Table of variables VIFs",align='center'),

      fluidRow(
        column(width = 6,
               h4("Before exclusion of collinear variables"),
               tableOutput("cor_all_VIF")
        ),

        column(width = 6,
               h4("After exclusion of collinear variables"),
               tableOutput("cor_rest_VIF")
        )
      ),
      tags$hr(style="border: 2px solid !important; color: #fcc201 !important;")
    ),

    shinydashboard::box(
      h3("Correlation matrix",align='center'),

      fluidRow(width='95%', dataTableOutput("cor_Mat"))
    )

  )

  out

})

# displays the user inferface allowing the user to enter settings and choose methods for correlations analysis
output$cor_analysis_args <- renderUI({

	if(is.null(input$cor_data_from_env_layers) || length(input$cor_data_from_env_layers)==0L || nchar(input$cor_data_from_env_layers)==0L
	   || length(input$cor_data_from_explvar)==0L)
		return()

	out = tagList()

			out=tagList(out,

				strong(p("Maximum number of observations :")),

				numericInput(inputId='cor_maxObs',

					label=NULL,

					value=5000,

					min=2,

					max=countNonNACells()
				),
				helpText("Number of rasters cells to use in computations. Max. value:",countNonNACells())
			)

			if(!is.null(input$cor_analysis) && input$cor_analysis=='PW_COR'){

				out= tagList(out,

					strong(p("Select a correlation threshold :")),

					numericInput(inputId='cor_pw_threshold',

						label=NULL,

						value=0.7,

						min=0,

						max=1,

						step=0.001
					),

					tags$style(appCSS),

					div(style="text-align: center",

						withBusyIndicatorUI(
							bsButton(inputId="run_pw_cor",

								label='Run',

								icon=icon("play", lib="font-awesome"),

								style='primary'
							)
						)
					),
					busyIndicator(ifelse(is.null(isolate(input$run_pw_cor))|| isolate(input$run_pw_cor)==0, "Loading...","Computations in progress..."),wait = 2000)
				)
			}

			if(!is.null(input$cor_analysis) && input$cor_analysis=='VIF_STEP'){

				out=tagList(out,

					strong(p("Select a VIF threshold :")),

					numericInput(inputId='cor_vifstep_threshold',

						label=NULL,

						value=10,

						min=1
					),

					numericInput(inputId='cor_vifstep_optional_threshold',

						label="Select a threshold for correlation (optional):",

						value=0.7,

						min=0,

						max=1,

						step=0.001
					),


					tags$style(appCSS),

					div(style="text-align: center",

						withBusyIndicatorUI(
							bsButton(inputId="run_vifstep",

								label='Detect',

								icon=icon("play", lib="font-awesome"),

								style='primary'
							)
						)
					),
					busyIndicator(ifelse(is.null(isolate(input$run_vifstep))|| isolate(input$run_vifstep)==0, "Loading...","Computations in progress..."),wait = 2000)
				)
			}

			if(!is.null(input$cor_analysis) && input$cor_analysis=='VIF_COR'){

				out=tagList(out,

					strong(p("Select a correlation threshold :")),

					numericInput(inputId='cor_vifcor_threshold',

						label=NULL,

						value=0.7,

						min=0,

						max=1,

						step=0.001
					),

					tags$style(appCSS),

					div(style="text-align: center",

						withBusyIndicatorUI(
							bsButton(inputId="run_vifcor",

								label='Detect',

								icon=icon("play", lib="font-awesome"),

								style='primary'
							)
						)
					),
					busyIndicator(ifelse(is.null(isolate(input$run_vifcor))|| isolate(input$run_vifcor)==0, "Loading...","Computations in progress..."),wait = 2000)
				)
			}

	wellPanel(style="background-color: lavender", out)
})
outputOptions(output,'cor_analysis_args',suspendWhenHidden=FALSE)

# displays results of the correlation analysis
output$cor_analysis_outputs <- renderUI({

	if(is.null(input$cor_analysis))
		return()

	myTabs <- lapply(c("Results","Table(s)","Details"),
				function(x){
					if(x=="Results"){
						tabPanel(x, icon = icon("list-alt", lib = "font-awesome"),
							uiOutput("cor_Report")
						)
					}# TODO: hide tables titles as long as tables are not created
					else if(x=="Table(s)" && input$cor_analysis!="PW_COR"){
						tabPanel(x, icon = icon("table", lib = "font-awesome"),
						    uiOutput("vif_tables")
						)
					}
					else if(x=="Table(s)" && input$cor_analysis=="PW_COR"){
						tabPanel(x, icon = icon("table", lib = "font-awesome"),

						  tags$hr(style="border: 2px solid !important; color: #fcc201 !important;"),

							h3("Correlation matrix",align='center'),

							dataTableOutput("cor_pw_Mat"),

							tags$hr(style="border: 2px solid !important; color: #fcc201 !important;")
						)
					}
					else if(x=="Details"){
						verbatimTextOutput("cor_Details")
					}
				}
			)
	do.call(tabsetPanel,myTabs)
})



















