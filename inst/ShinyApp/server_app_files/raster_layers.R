raster_processing_output_signal <- reactiveValues(region_cropped=TRUE, region_masked=TRUE, background_cropped=TRUE, background_masked=TRUE)

# --------------------------------------------------------

# REACTIVE FUNCTIONS

# --------------------------------------------------------

# returns raster stack with layers masked from background polygon shapefile
myBackgroundRasters <- reactive({

  # get the list of species training areas
  list_saved_background_extent 	<- isolate(reactiveValuesToList(background_extent_analysis_selected_extent))

  # get the training area selected
  selected_area 		<- if(is.null(isolate(input$niche_space_area))|| nchar(isolate(input$niche_space_area))==0L) NULL else isolate(input$niche_space_area)

  if(is.null(myRasters()) || sum(lengths(list_saved_background_extent))==0L || is.null(selected_area) || selected_area!="area_constrained")
    return(NULL)

	isolate({

		selected_origins 	<- if(is.null(input$niche_layers_from)|| nchar(input$niche_layers_from)==0L) rasters$origins else input$niche_layers_from

		selected_inputs   <- grep(".+_niche_space_area$",names(input),value=TRUE)

    selected_extent   <- if(length(selected_inputs)==0L) paste(unlist(sapply(list_saved_background_extent, function(x) paste(names(x),collapse="_"))),collapse="_") else paste(gsub(" ","_",unlist(reactiveValuesToList(input)[selected_inputs])), collapse="_")

		if(is.null(selected_origins) || length(selected_origins)==0L || nchar(selected_origins)==0L || length(selected_extent) == 0L || nchar(selected_extent)==0L)
			return(NULL)

    replacements = c("Drawn_on_map"="tool", "Pre-defined_region"="shp","Bounding_Box"="bbx","Convex_Hull"="hull","Environmnental_Profiling"="envprof","Circular_Buffers"="circle")
		fn 	<- fntmp <- sprintf("myRasters_%sBackground%s",selected_origins, stringr::str_replace_all(selected_extent,replacements))

		# check if a temporary object file already exists
		if("user_layers" %in% selected_origins) fntmp <- paste(fntmp, gsub("[\\|//|:]",'_',readDirectoryInput(session, 'rasters_layers_directory')),sep='_')
		if("wc_current_layers" %in% selected_origins) fntmp <- paste(fntmp, gsub("[\\|//|:]",'_',readDirectoryInput(session, 'WC_path_dir')),sep='_')
		fntmp <- file.path(dirname(rasterTmpFile()),paste0(fntmp,'.grd'))
		if(file.exists(fntmp)){
			R_rasters	<- raster::stack(fntmp)
			return(R_rasters)
		}

		fn <- fntmp

		  # first get the raster layers shared among all species
		  if(!is.null(myRegion())){
		    R_rasters 	<- myRegionRasters()
		  }else{
		    R_rasters 	<- myRasters()
		  }

		if(is.null(R_rasters))
		  return(NULL)

		cropmask_id <- shiny::showNotification(
		  ui=HTML("<div style='text-align: center; vertical-align: middle;'><span style='font-size: 18px;'><br>Cropping & masking<br>raster layers to <span class='label label-primary'>training area</span>...&nbsp;&nbsp;<img src='gifs/ajax-loader-bar.gif'></span><br></div>",
		          ), type="default", duration = NULL)

			# list of constraint methods
			constraint_method <- c("tool","shp","bbx","hull","envprof","circle")
      # species names
			species_names <- if(length(selected_inputs)>0L) stringr::str_extract(selected_inputs,".+?(?=_)") else names(list_saved_background_extent)

			# mask rasters
			B_rasters 	<- tryCatch({
			  raster::stack(
			    lapply(sort(species_names), function(sp){
			      if(!is.null(input[[paste0(sp,"_niche_space_area")]])){
  			        constraint <-  switch(input[[paste0(sp,"_niche_space_area")]],
  			                                  "Drawn on map"="tool",
  			                                  "Pre-defined region"="shp",
  			                                  "Bounding Box"="bbx",
  			                                  "Convex Hull"="hull",
  			                                  "Environmnental Profiling"="envprof",
  			                                  "Circular Buffers"="circle")
  			      }else{
  			        if(length(list_saved_background_extent[[sp]])==0L)
  			          return(R_rasters)
  			        constraint <- tail(names(list_saved_background_extent[[sp]]),1) # take the last background saved
  			      }
    			    if(constraint %in% constraint_method){
    			      background <- as(list_saved_background_extent[[sp]][[constraint]],"Spatial")
    			      if(length(species_names)==1){
    			        ShinyBIOMOD::maskCover(R_rasters, background) #raster::mask(raster::crop(R_rasters, background), background)
    			      }
    			      else
    			        ShinyBIOMOD::maskCover(R_rasters, background, do.crop=FALSE) # raster::mask(R_rasters, background)
    			    }else{
    			      R_rasters
    			    }
			      }
  			   )
			  )
			},
			error=function(err) return(NULL))

			shiny::removeNotification(id=cropmask_id)

			gc()

			if(is.null(B_rasters)){
			  shiny::showNotification(HTML("<span style='font-size: 18px;'><br>&nbsp;&nbsp;&nbsp;&nbsp;Cropping/masking <span class='label label-warning'>failed</span></span>"), type="warning", duration=5)
			  return(NULL)
			}

			if(inherits(B_rasters,c("RasterLayer","RasterStack")))
			  shiny::showNotification(HTML("<span style='font-size: 18px;'><br>&nbsp;&nbsp;&nbsp;&nbsp;Cropping/masking <span class='label label-success'>successful</span></span>"), type="default", duration=5)

			raster::writeRaster(B_rasters, filename = fn, overwrite=TRUE)

			return(B_rasters)
		#})


	})
})

# returns raster stack with layers cropped and masked from region polygon shapefile
myRegionRasters <- reactive({

	if(is.null(myRasters()) || is.null(myRegion()) || is.null(input$load_user_layers) || input$load_user_layers==0L)
		return(NULL)

	isolate({
		selected_origins <- if(is.null(input$niche_layers_from)|| nchar(input$niche_layers_from)==0L) rasters$origins else input$niche_layers_from
		selected_region  <- input$regionShape_files

		fn 	<- fntmp <- paste("myRegionRasters", selected_region, sep="_")

		# check if a temporary object file already exists
		if("user_layers" %in% selected_origins) fntmp <- paste(fntmp, gsub("[\\|//|:]",'_',readDirectoryInput(session, 'rasters_layers_directory')),sep='_')
		if("wc_current_layers" %in% selected_origins) fntmp <- paste(fntmp, gsub("[\\|//|:]",'_',readDirectoryInput(session, 'WC_path_dir')),sep='_')
		fntmp <- file.path(dirname(rasterTmpFile()),paste0(fntmp,'.grd'))
		if(file.exists(fntmp)){
			R_rasters	<- tryCatch(raster::stack(fntmp), error=function(err) return(NULL))
			return(R_rasters)
		}

		fn <- fntmp
		cropping_id <- shiny::showNotification(
		  ui=HTML("<div style='text-align: center; vertical-align: middle;'><span style='font-size: 18px;'><br>Cropping raster layers\nto <span class='label label-primary'>region</span> ...&nbsp;&nbsp;<img src='gifs/ajax-loader-bar.gif'></span><br></div>"),
		          type="default", duration = NULL
		)

		#disabled <- sapply(reactiveValuesToList(tabs)$names, shinyjs::disable)

		R_rasters 	<- try(raster::crop(myRasters(),myRegion()), silent=TRUE)

		shiny::removeNotification(id=cropping_id)

		if(inherits(R_rasters,"try-error")){
		  shiny::showNotification(HTML("<span style='font-size: 18px;'><br>&nbsp;&nbsp;&nbsp;&nbsp;Cropping <span class='label label-warning'>failed</span></span>"), type="warning", duration=5)
		  raster_processing_output_signal$region_cropped <- FALSE
		  #enabled <- sapply(reactiveValuesToList(tabs)$names, shinyjs::enable)
		  return(NULL)
		}

		masking_id <- shiny::showNotification(
		  ui=HTML("<div style='text-align: center; vertical-align: middle;'><span style='font-size: 18px;'><br>&nbsp;&nbsp;&nbsp;&nbsp;Masking raster layers<br>to <span class='label label-primary'>region</span>...&nbsp;&nbsp;<img src='gifs/ajax-loader-bar.gif'></span><br></div>"),
		  type="default", duration = NULL
		  )

		R_rasters 	<- try(ShinyBIOMOD::maskCover(raster::stack(R_rasters), myRegion(), do.crop=FALSE, filename=fn,overwrite=TRUE), silent=TRUE) #try(raster::mask(R_rasters,myRegion(),filename=fn,overwrite=TRUE), silent=TRUE)

		shiny::removeNotification(id=masking_id)
		#enabled <- sapply(reactiveValuesToList(tabs)$names, shinyjs::enable)

		if(inherits(R_rasters,c("RasterLayer","RasterStack")))
		  shiny::showNotification(HTML("<span style='font-size: 18px;'><br>&nbsp;&nbsp;&nbsp;&nbsp;Cropping & masking <span class='label label-success'>successful</span></span>"), type="default", duration=5)
    else{
      shiny::showNotification(HTML("<span style='font-size: 18px;'><br>&nbsp;&nbsp;&nbsp;&nbsp;Cropping & masking <span class='label label-warning'>failed</span></span>"), type="warning", duration=5)
      if(inherits(R_rasters,"try-error"))
        R_rasters <- NULL
      raster_processing_output_signal$region_masked <- FALSE
    }
		return(R_rasters)
	})
})

# returns user uploaded raster layers and combined them in unique stack object
myRasters  <- reactive({

	selected_origins <- if(is.null(input$niche_layers_from)|| nchar(input$niche_layers_from)==0L) rasters$origins else input$niche_layers_from

	if(is.null(selected_origins))
		return(NULL)

	fn 		<- fntmp <- paste("myRasters", selected_origins, sep="_")
	rasters <- raster::stack()

	isolate({
		# check if a temporary object file already exists
		if("user_layers" %in% selected_origins) fntmp <- paste(fntmp, gsub("[\\|//|:]",'_',readDirectoryInput(session, 'rasters_layers_directory')),sep='_')
		if("wc_current_layers" %in% selected_origins) fntmp <- paste(fntmp, gsub("[\\|//|:]",'_',readDirectoryInput(session, 'WC_path_dir')),sep='_')
		fntmp <- file.path(dirname(rasterTmpFile()),fntmp)
		if(file.exists(fntmp)){
			rasters	<- tryCatch(raster::stack(rasters, get(load(fntmp))), error=function(err) return(NULL))
			return(rasters)
		}

		# combine different source of raster layers
		if ("user_layers" %in% selected_origins ){
			rasters = tryCatch(raster::stack(rasters, rasterLayers()), error=function(err) return(NULL))
			fn <- paste(fn, gsub("[\\|//|:]",'_',readDirectoryInput(session, 'rasters_layers_directory')),sep='_')
		}
		if("wc_current_layers" %in% selected_origins ){
			raster_wc_current <- raster_worldclim_current()
			fn <- paste(fn, gsub("[\\|//|:]",'_',readDirectoryInput(session, 'WC_path_dir')),sep='_')

			if(!is.null(rasters) && raster::nlayers(rasters)>0){
  			# check here for extent and resolution
  			if(raster::res(rasters)[1]!=raster::res(raster_wc_current)[1])
  				raster_wc_current <- raster::resample(raster_wc_current, rasters[[1]])
  			else if(!identical(raster::extent(rasters),raster::extent(raster_wc_current))){
  				id.maxcells = which.max(c(raster::ncell(rasters),raster::ncell(raster_wc_current)))
  				if(id.maxcells==1) rasters <- raster::crop(rasters, raster_wc_current[[1]]) else raster_wc_current <- raster::crop(raster_wc_current,rasters[[1]])
  			}
  			rasters = tryCatch(raster::stack(rasters, raster_wc_current), error=function(err) return(NULL))
      }else
        rasters = raster_wc_current
		}

		if(is.null(rasters) || raster::nlayers(rasters)<1)
			return(NULL)

		save(rasters, file=file.path(dirname(rasterTmpFile()),fn))

		return(rasters)
	})
})

# returns environmental raster layers according to user defined training area
myRasterStack <- reactive({

	selected_area <- if(is.null(input$niche_space_area)|| nchar(input$niche_space_area)==0L) rasters$area else input$niche_space_area

	if(!is.null(selected_area)){

	  if(selected_area=='all_area')
	    return(myRasters())

	  if(selected_area=='area_of_interest')
	    return(myRegionRasters())

	  if(selected_area=='area_constrained'){
	    return(myBackgroundRasters())
	  }

	}
	return(NULL)
})
