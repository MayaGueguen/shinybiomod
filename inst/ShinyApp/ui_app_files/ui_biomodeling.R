
ui_biomodmodeling <- fluidPage(

					useShinyjs(),

					extendShinyjs("www/js/app-shinyjs.js", functions = c("getInputType")),

					includeScript("www/js/shinyBS.js"),

					titlePanel(h1(strong(em("BioMod 2")), style = "font-family: 'times', cursive; font-weight: 500; line-height: 1.1;")),

					sidebarLayout(position = "left",

						sidebarPanel(

							tags$head(
								tags$style(HTML('#save_biomodeling_parms{color: black; background-color:white; border-color:black;}')),
								tags$style(HTML('#reset_biomodeling_page{color: black; background-color:white; border-color:black;}')),
								tags$script(src = 'http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML', type = 'text/javascript',includeScript("style/google-analytics.js"))
							),

							hr(),

							div(style="display:inline-block; vertical-align: top;", h3(strong("Step 2 of 4 :"), style ="color: black")),
							div(style="display:inline-block; vertical-align: top;", h3(strong("Modelling species distribution"))),

							div(style="text-align: justify",
								p(em("Calibrate and evaluate a range of"), strong(em("species distribution models algorithms")),em("for a given set of species using the "),a(href="https://cran.r-project.org/web/packages/biomod2/biomod2.pdf",strong(em("biomod2"))),em("package.")),
								tags$ul(style="font-family: 'times';font-size: 18px;",
								        tags$li("Calibration can be made by",strong(em("cross-validation")),em("(see NbRunEval and Datasplit arguments))",".")),
								        tags$li("A range of",strong(em("evaluation metrics")),"can be used for assessing the predictive capacity of your model(s).")
								)
							),
							#p("Calibrations are made on the whole dataset (if",em("DataSplit = 100%"),") or a random subsample of it (i.e.",em("DataSplit < 100%"),"), and evaluations are made on the remaining sample. This",strong(em("cross-validation")),"process can be replicated several times",em("(see NbRunEval)"),"."),
							#p("The predictive power of each different models is estimated using a range of",strong(em("evaluation metrics")),em("(see Evaluation metrics)"),", and the overall predictive power can be assessed later by summarizing those evaluation metrics across the different models and replicated runs.")

							#div(style="display:inline-block; vertical-align: top;", h5("Click the 'refresh' icon to show last output")),
							#div(style="display:inline-block; vertical-align: top;", bsButton(inputId="refresh_biomodeling_page", icon = icon("refresh"), label = NULL, style = "primary", size="extra-small")),

							#div(id='biomodeling_page',

									# div(align='center',
										# bsButton(inputId="refresh_biomodeling_page",icon = icon("refresh"), label = "Click to refresh the page output",	style = "primary")
									# ),

								hr(),

								h3("Settings"),

								uiOutput("chooserModelingSpecies"),

								br(),

								wellPanel(

									hidden(
										div(id="Load_saved_biomod_modeling_parms", align='center',
											radioButtons(inputId='load_biomodeling_parms',label=shiny::span(icon('question-circle',lib="font-awesome"), strong('Load saved settings ?'), style='font-size: 18px'),choices=c('YES', 'NO'), selected="NO", inline=TRUE),
											br()
										)
									),

									# div(align='center',

										# bsButton(inputId="load_biomodeling_parms", icon = icon("upload"), label = "Load saved parameters",	style = "primary")

									# ),

									h4(strong('Modeling ID')),

									textInput(inputId='biomod_modeling_id',

										label=NULL,

										value='FirstModeling',

										placeholder='Give a name to your modeling process'
									),

									br(),

									h4(strong("Models")),

									selectInput(inputId='biomod_Models',

										label = NULL,

										choices = c('GLM'='GLM','GBM'='GBM', 'GAM'='GAM','CTA'= 'CTA','ANN'= 'ANN', 'SRE'='SRE', 'FDA'='FDA', 'MARS'='MARS','RF'= 'RF', 'MAXENT (Phillips)'='MAXENT.Phillips'),

										multiple=TRUE,

										selected= c('GAM','RF')
									),
									ShinyBIOMOD::selectizePopover(id='biomod_Models', choice='GLM',
									                              title='<strong>Generalized Linear Model</strong>',
									                              content='<p>A regression-type model which uses <i>parametric terms</i> to model the response form of species to a set of predictors</p>',
									                              placement = "right", trigger = "click",options = list(container = "body", delay = list(show=1000, hide=3000))),

									ShinyBIOMOD::selectizePopover(id='biomod_Models', choice='GBM',
									                              title='<strong>Generalized Boosting Model</strong>',
									                              content='<p>A machine learning-type model (also called <i>Boosted Regression Trees</i>) which combines decision trees and boosting</p>',
									                              placement = "right", trigger = "click",options = list(container = "body", delay = list(show=1000, hide=3000))),

									ShinyBIOMOD::selectizePopover(id='biomod_Models', choice='GAM',
									                              title='<strong>Generalized Additive Model</strong>',
									                              content='<p>A regression-type model which uses flexible smoothed functions to fit <i>non-linear relationships</i> between a set of predictors and the species response.</p>',
									                              placement = "right", trigger = "click",options = list(container = "body", delay = list(show=1000, hide=3000))),

									ShinyBIOMOD::selectizePopover(id='biomod_Models', choice='CTA',
									                              title='<strong>Classification Tree Analysis</strong>',
									                              content='<p>A machine learning-type model which uses <i>repeated partitioning</i> of predictors to predict species response.</p>',
									                              placement = "right", trigger = "click",options = list(container = "body", delay = list(show=1000, hide=3000))),

									ShinyBIOMOD::selectizePopover(id='biomod_Models', choice='ANN',
									                              title='<strong>Artificial Neural Network</strong>',
									                              content='<p>A machine learning-type model which uses a <i>network of weighted combinations</i> of input predictors to predict output species response.</p>',
									                              placement = "right", trigger = "click",options = list(container = "body", delay = list(show=1000, hide=3000))),

									ShinyBIOMOD::selectizePopover(id='biomod_Models', choice='SRE',
									                              title='<strong>Surface Range Envelop</strong>',
									                              content='<p>An environmental-envelop based model which uses the <i>dimensions of environmental space</i> of predictors to predict species occurrence.</p>',
									                              placement = "right", trigger = "click",options = list(container = "body", delay = list(show=1000, hide=3000))),

									ShinyBIOMOD::selectizePopover(id='biomod_Models', choice='FDA',
									                              title='<strong>Flexible Discriminant Analysis</strong>',
									                              content='<p>A classification-based model which uses <i>non-linear combinations</i> of predictors and optimal scoring to predict the species occurrence.</p>',
									                              placement = "right", trigger = "click",options = list(container = "body", delay = list(show=1000, hide=3000))),

									ShinyBIOMOD::selectizePopover(id='biomod_Models', choice='MARS',
									                              title='<strong>Multiple Adaptive Regression Splines</strong>',
									                              content='<p>A regression-type model which uses <i>multiple piecewise basis functions</i> to fit complex non-linear relationships between a set of predictors and the species response.</p>',
									                              placement = "right", trigger = "click",options = list(container = "body", delay = list(show=1000, hide=3000))),

									ShinyBIOMOD::selectizePopover(id='biomod_Models', choice='RF',
									                              title='<strong>Random Forest</strong>',
									                              content='<p>A machine learning-type model which uses <i>averages of multiple classification and regression trees</i> constructed from bootstrapped samples of the data</p>',
									                              placement = "right", trigger = "click",options = list(container = "body", delay = list(show=1000, hide=3000))),
									br(),

									h4(strong("Models options")),

									dropdown(

									  h3("Model options"),

									  p("Set the different options for each modeling technique selected"),

									  uiOutput("biomod_models_options_panel"),

									  status="danger", icon=icon("cog"), width='100%', tooltip = tooltipOptions(title = "Click to set model options !")
									),
									#div(style="display:inline-block; vertical-align: top;", h5("Click here to set model(s) parameters")),
									#div(style="display:inline-block; vertical-align: top;", bsButton(inputId="biomod_models_set_options", icon = icon("directions"), label = NULL, style = "primary", size="extra-small")),

									br(),

									h4(strong("NbRunEval")),

									numericInput(inputId='biomod_NbRunEval',

										label=NULL,

										value=1,

										min=1
									),
									helpText('Number of running evaluation. Default: 1'),

									br(),

									h4(strong("DataSplit")),

									numericInput(inputId='biomod_DataSplit',

										label=NULL,

										value=70,

										min=1,

										max=100
									),
									helpText('Percentage of training data (%). Default: 70'),

									br(),

									h4(strong("Yweights")),

									fileInput(inputId='biomod_Yweights',

										label=NULL,

										accept=".asc"
									),
									helpText('Upload an ascii file containing weights of presence data. Default is NULL'),

									br(),

									h4(strong("Prevalence")),

									numericInput(inputId='biomod_Prevalence',

										label=NULL,

										value=0.5,

										min=0,

										max=1,

										step=0.001
									),

									br(),

									h4(strong("Variable Importance")),

									numericInput(inputId='biomod_Modeling_VarImport',

										label=NULL,

										value=NULL,

										min=1
									),
									helpText('Number of permutation (resampling) to estimate importance of explanatory variables.'),

									br(),

									h4(strong("Evaluation metrics")),

									selectInput(inputId='biomod_EvalMetric',

										label = NULL,

										choices = c('KAPPA'='KAPPA', 'TSS'='TSS', 'ROC'='ROC', 'FAR'='FAR', 'SR'='SR', 'ACCURACY'='ACCURACY', 'BIAS'='BIAS', 'POD'='POD', 'CSI'='CSI' ,'ETS'='ETS'),

										multiple=TRUE,

										selected=c('TSS','ROC')
									),
									# ShinyBIOMOD::selectizePopover(id='biomod_EvalMetric', choice='TSS',
									#                               title='<strong>True Skill Statistic</strong><i> (Hanssen and Kuipers discriminant, Peirce&#146s skill score)</i>',
									#                               content="<p>Describes how well the model separates the species presences from the species absences.<br><strong>Range:</strong> 0 to 1, 1 indicates perfect score.</p>",
									#                               placement = "right", trigger = "hover",options = list(container = "body")),
									#
									# ShinyBIOMOD::selectizePopover(id='biomod_EvalMetric', choice='KAPPA',
									#                               title='<strong>Cohen&#146s Kappa</strong><i> (Heidke skill score)</i>',
									#                               content='<p>Measures the accuracy of the prediction relative to that of random chance.<br><strong>Range:</strong> -1 to 1, 1 indicates perfect score.</p>',
									#                               placement = "right", trigger = "hover",options = list(container = "body")),
									#
									# ShinyBIOMOD::selectizePopover(id='biomod_EvalMetric', choice='ROC',
									#                               title='<strong>Relative Operating Characteristic</strong>',
									#                               content='<p>Measures how well the model discriminates the species presences from the species absences.<br><strong>Range:</strong> 0 to 1, 1 indicates perfect score.</p>',
									#                               placement = "right", trigger = "hover",options = list(container = "body")),
									#
									# ShinyBIOMOD::selectizePopover(id='biomod_EvalMetric', choice='FAR',
									#                               title='<strong>False Alarm Ratio</strong>',
									#                               content='<p>Measures the proportion of observed absences incorrectly predicted (present).<br><strong>Range:</strong> 0 to 1, 0 indicates perfect score.</p>',
									#                               placement = "right", trigger = "hover",options = list(container = "body")),
									#
									# ShinyBIOMOD::selectizePopover(id='biomod_EvalMetric', choice='SR',
									#                               title='<strong>Success Ratio</strong>',
									#                               content='<p>Measures the proportion of observed species presences correctly predicted.<br><strong>Range:</strong> 0 to 1, 1 indicates perfect prediction.</p>',
									#                               placement = "right", trigger = "hover",options = list(container = "body")),
									#
									# ShinyBIOMOD::selectizePopover(id='biomod_EvalMetric', choice='ACCURACY',
									#                               title='<strong>Accuracy</strong><i> (fraction correct)</i>',
									#                               content='<p>Measures the overall proportion of observed species presences and absences that were correctly classified.<br><strong>Range:</strong> 0 to 1, 1 indicates perfect score.</p>',
									#                               placement = "right", trigger = "hover",options = list(container = "body")),
									#
									# ShinyBIOMOD::selectizePopover(id='biomod_EvalMetric', choice='BIAS',
									#                               title='<strong>Bias score</strong><i> (frequency bias)</i>',
									#                               content='<p>Measures the frequency of the predicted species presences compared to that of observed presences.<br><strong>Range:</strong> 0 to Inf, BIAS<1 indicates underprediction, BIAS>1 overprediction and 1 no bias.</p>',
									#                               placement = "right", trigger = "hover",options = list(container = "body")),
									#
									# ShinyBIOMOD::selectizePopover(id='biomod_EvalMetric', choice='POD',
									#                               title='<strong>Probability Of Detection</strong><i> (Hit rate)</i>',
									#                               content='<p>Measures the proportion of species presences that were correctly predicted.<br><strong>Range:</strong> 0 to 1, 1 indicates perfect score.</p>',
									#                               placement = "right", trigger = "hover",options = list(container = "body")),
									#
									# ShinyBIOMOD::selectizePopover(id='biomod_EvalMetric', choice='CSI',
									#                               title='<strong>Critical Success Index</strong><i> (Threat score)</i>',
									#                               content='<p>Describes how well the species presences are correctly predicted.<br><strong>Range:</strong> 0 to 1, 1 indicates perfect score.</p>',
									#                               placement = "right", trigger = "hover",options = list(container = "body")),
									#
									# ShinyBIOMOD::selectizePopover(id='biomod_EvalMetric', choice='ETS',
									#                               title='<strong>Equitable Threat Score</strong><i> (Gilbert Skill Score)</i>',
									#                               content='<p>Measures the proportion of observed species presences that were correctly predicted,<br>adjusted to that of random chance.<br><strong>Range:</strong> -1/3 to 1, 1 indicates perfect prediction.</p>',
									#                               placement = "right", trigger = "hover",options = list(container = "body")),
									br(),

									checkboxInput(inputId='biomod_rescale_all_models',

										label = strong('rescale all models'),

										value=FALSE
									),

									br(),

									checkboxInput(inputId='biomod_do_full_models',

										label = strong('do full models'),

										value=FALSE
									)

								),
							#),

							div(style="display:inline-block; vertical-align: top; width: 30%;",HTML("<br>")),
							div(style="display:inline-block; vertical-align: top; #save_biomodeling_parms{color: black; background-color: white; border-color: black;}",bsButton(inputId="save_biomodeling_parms", icon = icon("hdd", lib='glyphicon'), label = "Save", size="small")),
							div(style="display:inline-block; vertical-align: top;",bsButton(inputId="reset_biomodeling_page", icon = icon("eraser"), label = "Clear", style = "btn-primary", size="small")),

							hidden(

							  div(id="biomod_modeling_parComputation_option",
    							awesomeCheckbox(inputId='biomod_modeling_parComputation',

    								label = strong('allow parallel computations'),

    								value=FALSE,

    								status = "primary"
    							),
    							helpText("You must have administrator rights on your computer")
							  )

							),

							br(),

							awesomeCheckbox(inputId="biomod_modeling_only_active_tab", label=strong("run current species only"), value = TRUE, status = "primary"),
							helpText('Note: recommended if the user wants to model each species with different parameters'),

							tags$style(appCSS),

							useShinyjs(),

							div(align='center',

								withBusyIndicatorUI(
									bsButton(inputId="biomod_run_modeling",

									icon = icon("play", lib="font-awesome"),

									label = "Run BIOMOD Modeling"#,

									#style = "primary"
									)
								)
							),

							hr()

						),

						mainPanel(

							uiOutput("biomod_modeling_panelSpecies")

						)

					)

)
