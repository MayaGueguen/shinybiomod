pca_analysis <- fluidPage(

							titlePanel(h1(strong(em("Principal Component Analysis")), style = "font-family: 'times', cursive; font-weight: 500; line-height: 1.1;")),

							sidebarLayout(position = "left",

								sidebarPanel(

									tags$head(

									  tags$script(src = 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML', type = 'text/javascript')

									),

									hr(),

									div(style="text-align: justify;",
										p("Reduce your environmental space into synthetic",em("principal components"),"given by",em(strong("PCA")),"axis scores with the ",
										  a(href="https://cran.r-project.org/web/packages/ade4/ade4.pdf",strong(em("ade4"))),em("package.")),
										tags$ul(
										tags$li(p("Plot the",strong(em("contribution of variables")),"and find which environmental variable(s) best describe(s) your dataset.")),
										tags$li(p("Map the",strong(em("contribution of raster cells")),"and visualize geographical areas alike environmental conditions found in your dataset."))
										)
									),

									hr(),

									h3("Data"),

									strong(p("Select a study area")),

									selectizeInput(inputId="pca_data_from_area",

										label = NULL,

										choices = NULL,

										options = list(placeholder = "Select area extent")

									),

									strong(p("Select environmental data")),

									selectizeInput(inputId="pca_data_from_env_layers",

										label = NULL,

										choices = NULL,

										multiple = TRUE,

										options = list(placeholder="Select raster layers dataset(s)")
									),

									strong(p("Select variables")),

									selectizeInput(inputId="pca_data_from_explvar",

										label = NULL,

										choices = NULL,

										multiple = TRUE,

										options = list(placeholder="Select explanatory variables")
									),

									hr(),

									hidden(
										div(id="pca_analysis_panel_settings",
											uiOutput("pca_analysis")
										)
									),

									hidden(

										div(id="pca_diagnostic_options",

											h3("Diagnostics"),

											awesomeCheckbox(inputId="pca_eigenValues_inertia",

												label="Contribution of principal components",

												value = TRUE,

												status = "primary"
											),
											helpText("Note: compute the proportion of variance explained by an axis"),

											uiOutput('pca_eigenValues_out'),

											br(),

											awesomeCheckbox(inputId="pca_contribVar",

												label="Contribution of variables",

												value = TRUE,

												status = "primary"
											),
											helpText("Note: compute the contribution of each variables in the building of an axis"),

											br(),

											awesomeCheckbox(inputId="pca_projectVar",

												label="Factor plan of variables",

												value = TRUE,

												status = "primary"
											),
											helpText("Note: project variables on factor plan (biplot)"),

											br(),

											awesomeCheckbox(inputId="pca_projectObs",

												label="Factor map of observations",

												value = FALSE,

												status = "primary"
											),
											helpText("Note: map the contribution of raster cells to the building of an axis"),

											hr()
										)
									)

								),

								mainPanel(

									uiOutput("pca_analysis_outputs")

								)
							)
						)
