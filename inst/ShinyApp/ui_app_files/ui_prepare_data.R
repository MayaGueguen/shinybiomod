ui_prepare_data <- fluidPage(

							useShinyjs(),

							extendShinyjs("www/js/app-shinyjs.js", functions = c("getInputType")),

							titlePanel(h1(strong(em("Data preparation")), style = "font-family: 'times', cursive; font-weight: 500; line-height: 1.1;")),

							sidebarLayout(position = "left",

								sidebarPanel(

									tags$head(
									  tags$style(".shiny-progress {top: 80% !important;left: 80% !important;margin-top: -150px !important;margin-left: -300px !important; color: blue;font-size: 20px;font-style: serif;}"),
										tags$style(HTML('#Save_extract{color:black; background-color: none; border-color: black}')),
										tags$style(HTML('#Save_env{color:black; background-color: white; border-color: black}'))
									),

									p("Choose the",strong("geographic area"),", the",strong("set of occurrence records"),", and ", strong("environmental variables"),"with which you want to train your model(s).",br(),
									  em("You can still return back to the previous sections and process your data as you wish.")),

									hr(),

									strong(p("Select a training area")),

									selectizeInput(inputId="niche_space_area",

										label = NULL,

										choices = NULL,

										options=list(placeholder="Select area extent")

									),

									hidden(

										div(id="data_preparation_background_extent_use",
											uiOutput('data_preparation_background_selector')
										)
									),

									strong(p("Select training occurence data")),

									selectizeInput(inputId="occ_dat",

										label = NULL,

										choices = NULL,

										options=list(placeholder="Select a dataset")
									),

									hidden(
										div(id="data_preparation_thinned_occ_dat_selector",

											p('Which species would you like to select ?'),

											selectizeInput(inputId="thinned_occ_dat",

												label = NULL,

												choices = NULL,

												multiple=TRUE,

												options=list(placeholder="Select one or more species")
											)
										)
									),

									strong(p("Select environmental data")),

									selectizeInput(inputId="niche_layers_from",

										label = NULL,

										choices = NULL,

										multiple=TRUE,

										options=list(placeholder="Select raster data")
									),

									hidden(

										div(id="niche_layers_explvar_selector",

											p("Which variables would you like to select ?"),

											hidden(
  											div(class='info', id="niche_layers_explvar_suggested_info",
  											    p(icon("lightbulb",lib="font-awesome"),em("Among the set of environmental variables:")),
  											    radioButtons(inputId='suggest_explvar',label=NULL, choices=c('Available'='all'), inline=TRUE)
  											)
											),

											selectizeInput(inputId="niche_layers",

												label = NULL,

												choices = NULL,

												multiple = TRUE,

												options = list(placeholder="Select explanatory variables")
											)
										)

									),

									tags$style(appCSS),

									checkboxInput(inputId = 'clean_rep',

										label=shiny::span(icon('broom',lib="font-awesome"),strong('Remove duplicated records')),

										value=TRUE
									),

									hidden(
										div(id="extract_button_div", align='center',
                          withBusyIndicatorUI(bsButton(inputId="run_extract", label = "Prepare", icon=icon("tasks", lib="font-awesome"), style = "primary"))
									  )
									),

									div(id="busy_indicator_data_extraction_loading",
										busyIndicator("Loading..",wait = 3000)
									),

									hidden(
										div(id="busy_indicator_data_extraction",
											busyIndicator("Extracting..",wait = 0)
										)
									),

									br(),hr()

							),

              mainPanel(

                useShinyjs(),

                withSpinner(leafletOutput(outputId="PreparedMap", width = "100%", height="700px"),type=5,size=1, color='#000000'),

                hidden(
                  div(id="data_preparation_save_options",
                      tags$style(appCSS),
                      tags$hr(style="border: 2px solid !important; color: #fcc201 !important;"),
                      fluidRow(

                        shinydashboard::box(
                          h3("Saving your training data"),
                          br(),
                          p("If you are satisfied with the delimitation of your training area, the distribution of your occurrence records",
                            ", and the set of environmental variables, please",strong("do not forget to save your datasets"), "if you want to train your model(s) with these data in the next section.",
                            br(),br(), "Here save your:"),
                          tags$ul(
                            tags$li(div(style="display:inline-block; vertical-align: top; #Save_extract{color:black; background-color: none; border-color: black};.fa-map-marker-alt{ color: red;}", withBusyIndicatorUI(bsButton(inputId="Save_extract", icon = icon("map-marker-alt", lib="font-awesome"), label = "occurrence records")))),
                            tags$li(div(style="display:inline-block; vertical-align: top;", withBusyIndicatorUI(bsButton(inputId="Save_env", icon = icon("fas fa-cloud-sun-rain", lib="font-awesome"), label = "environmental rasters"))))
                          )
                        ),

                       shinydashboard::box(
                        h3("Occurrences with data"),
                        br(),
                        dataTableOutput("dataM"),
                        radioButtons(inputId='format',label=strong('Format'),choices=c('CSV'="csv", 'TXT'="txt"), selected="csv", inline=TRUE),
                        div(style="display:inline-block; vertical-align: top;", downloadButton(outputId = "download_extraction",label = "download"))
                       )

                    ),
                    tags$hr(style="border: 2px solid !important; color: #fcc201 !important;")
                  )
                )

              )
            )
)
