ui_projMap <-div(class="outer",

				tags$head(
					includeCSS("style/styles.css"), includeScript("style/gomap.js")
				),

				leafletOutput(outputId="projMap", width = "100%", height="100%"),#withSpinner(,type=5,size=3, color='#000000'),

				uiOutput("projMapPanelOptions")

			)
