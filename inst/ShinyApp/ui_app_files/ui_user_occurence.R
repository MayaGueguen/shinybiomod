user_occ <- fluidPage(

			useShinyjs(),

			extendShinyjs("www/js/app-shinyjs.js", functions = c("getInputType")),

			# https://community.rstudio.com/t/where-is-updatefileinput/11999/3

			extendShinyjs(text = "shinyjs.clearUpload = function(paramsId) {$('#' + paramsId).parent().parent().next()[0].value = ''}",
			              functions=c("clearUpload")),

			## scripts from shinydashboard to make box collapsible in shiny
			##includeCSS(system.file("AdminLTE/AdminLTE.css", package="shinydashboard")),

			includeCSS(system.file("shinydashboard.css", package="shinydashboard")),

			includeScript(system.file("AdminLTE/app.js", package="shinydashboard")),
      ##

			#includeScript("style/gomap.js"),

			fluidRow(
			  column(8,
			    div(style="display:inline-block; vertical-align: top;",
			    titlePanel(h1(strong(em("Occurrence data")), style = "font-family: 'times', cursive; font-weight: 500; line-height: 1.1;"))
			    )
			 ),
			 #div(style="display:inline-block; vertical-align: top; width: 70%;",HTML("<br>")),
  			column(4,br(),
  			  hidden(
  			    div(id="show_dmap_occ_data",style="display:block; vertical-align: top; text-align: right",
  			      render_reportUI("dmap_occ_data")
  			    )
  			  )
  			)
			),

			sidebarLayout(position = "left",

						sidebarPanel(

						  tags$head(
						    tags$style(HTML("#shiny-notification-panel {bottom: 8px; right: 60px;} .shiny-notification{width: 300px; height: 100px; opacity: 1; border: 1px solid; color: black;}"))
						  ),

							hr(),

							h3("Data import"),

							p(strong("Upload your species dataset")),

							#radioButtons(inputId='format',label=strong('Format'),choices=c('CSV', 'TXT'), selected="TXT"),
							#div(style="display: inline-block; vertical-align: top;",
							    uiOutput('fileInput'),
							#),
							#div(style="display: inline-block; vertical-align: top; width: 25%;",
							    #bsButton(inputId="resetFileInput", label=NULL, icon=icon("times", lib="font-awesome"))
							#),

							div(id="tip_user_occurrence_data_format",

  							div(class="warning",p(style="color:#8d5524; padding-top: 0.5em; padding-bottom: 0.5em; padding-left: 0.5em; border: 1px; border-color:#8d5524;",icon("exclamation-triangle",lib="font-awesome"),em("Please make sure your dataset is formatted as shown in the example below. "),
  							    shiny::actionLink(inputId="tip_user_occurrence_data_format_got_it", label=strong("Got it !",style="color:#8d5524;")))),

  							div(align='center',
  							    tags$img(src='ShinyBiomod_data_format.png', height='75%', width='80%', style="background-color: white;")
  							),

  							helpText(em("Note: this dataset contains occurrence records from N species. The names of the columns can be different than the ones shown in the example"))
              ),

							div(id="species_with_data_question", align='center', radioButtons(inputId='Is_occ_with_env_data',label=shiny::span(icon('question-circle',lib="font-awesome"),strong('Occurrence records with environmental predictors ?'), style='font-size: 18px'),choices=c('YES', 'NO'), selected="NO", inline=TRUE)),

							hr(),

							# hidden(
							# 	div(id="busy_indicator_user_occurrences_loading",
							# 		busyIndicator("Updating dataset...",wait = 0)
							# 	)
							# ),

							shinyjs::hidden(

								div(id='occ_data_settings',
									h3("Data settings"),

									p(strong("Set the data column names appropriately ")),

									selectInput(inputId='User_Species_name', label='species column name', choices=NULL),

									selectInput(inputId='User_XLongitude', label= 'longitude column name', choices=NULL),

									selectInput(inputId='User_YLatitude', label='latitude column name', choices=NULL),

									div(id='user_environmental_data_column_names',selectizeInput(inputId='User_EnvData', label='environmental predictor(s) column name(s)', choices=NULL, multiple=TRUE)),

									div(align='center', radioButtons(inputId='Is_it_PA_data',label=shiny::span(icon('question-circle',lib="font-awesome"),strong('Presence-Abscence data ?'), style='font-size: 18px'),choices=c('YES', 'NO'), selected="NO", inline=TRUE)),

									div(id="user_occurence_pa_data",

										selectizeInput(inputId='User_Species_indicator_variable',
											label=strong('Which column indicates the Presence/Abscence of your species ?'),
											choices=NULL,
											options=list(placeholder="Select your indicator variable")
										),

										div(id="user_occurence_pa_data_presence_values",
											selectizeInput(inputId='User_Species_presence_values',
												label=strong('Which category(ies) correspond(s) to presences ?'),
												choices=NULL,
												multiple=TRUE,
												options=list(placeholder="Select one or more presence value(s)")
											),
											shiny::span(id='user_occurence_pa_data_presence_values_outcome_warning', style='text-align: center',p(icon('exclamation-circle',lib='font-awesome'), "Please select a variable with at least 2 distinct outcome"), style='color: red;')
										)
									),

									hr(),

									h3("Data selection"),

									p(strong("Select your species")),

									uiOutput("chooserInputSpecies"),

									br(),br(),

									div(id="occ_data_selectbyattributes",

										div(align='center', radioButtons(inputId='Select_occ_data_by_attributes',label=shiny::span(icon('question-circle',lib="font-awesome"),strong('Select by attributes ?'), style='font-size: 18px'),choices=c('YES', 'NO'), selected="NO", inline=TRUE)),

										div(id='Select_occ_data_by_attributes_yes',

											p(strong("Set the column name of your attribute variable")),

											selectInput(inputId='User_Group_name', label='attribute column name', choices=NULL),

											p(strong("Select your attribute values")),

											uiOutput("chooserInputLocations"),

											br()
										)
									),
									tags$style(appCSS),

									useShinyjs(),

									div(align='center', withBusyIndicatorUI(actionButton(inputId="User_Select_data",label="Subset dataset", styleclass="primary"))),

									hr()
								)
							)
						),

						mainPanel(


							tabsetPanel(id="user_occ_tab_panels",

								tabPanel("Tables", value="user_occ_tables_tab", icon = icon("table", lib = "font-awesome"),

  				        div(class="info",id="user_species_occurrences_info",style="border: 2px solid #e7f3fe; text-align: justify;",
  				             br(),
  				             p(style="font-size: 16px; padding-top: 0.5em; padding-bottom: 0.5em;", shiny::span(icon('info-circle',lib="font-awesome"),
  				            "Please upload your data from the left-hand side panel and make sure to follow the format required."))
  				        ),
								  hidden(

  								    div(id="user_species_occurrences_tables_panel",

  								      fluidRow(

  								        shinydashboard::box(status="warning",
  								            h3("Summary"),
                              div(id="user_occurrence_data_summary",  uiOutput("user_occurrences_data_summary")),
        								      div(id="user_occurrence_data_actionlinkButton", HTML('&nbsp;'), HTML('&nbsp;'),
        								          withBusyIndicatorUI(actionLink(inputId="user_occurrence_data_summary_readmoreButton",label=strong("Read more...")))
        								      )
    								        ),
  								        shinydashboard::box(status="warning",
  								              h3("Input data"),
    								            withSpinner(DTOutput(outputId="Data_Species_Table"),type=5,size=0.5, color='#000000')
    								        )

  								        ),

  								      fluidRow(

  					               shinydashboard::box(status="warning",
  					                h3("Number of records by species"),

          									withSpinner(tableOutput(outputId="Records_Species_Table"),type=5,size=0.5, color='#000000')
  								        )
  								      )

  								    )
								  )

								),

								#hidden(

                  #div(id="user_occ_map_tab",

  								tabPanel("Map", value="user_occ_map_tab", icon = icon("map-marker-alt", lib = "font-awesome"),

  								         # shinyjs::hidden(
  								         #
  								         #   shiny::tagList(
  								         #
  								         #     shiny::div(class="warning",id='occurrence_map_warning_message',style="text-align: justify;",
  								         #                shiny::p(style="tetx-align:justify; padding-left: 1em; padding-top: 1em; padding-bottom: 1em; padding-right: 1em; color:#8d5524", icon('exclamation-triangle',lib='font-awesome'),
  								         #                         "This dataset contains a huge number of occurrence points, what may significantly reduce the performance of the interactive map.",
  								         #                         "You can improve the performance of your navigation by randomly selecting a sample of the records to map (expressed as a proportion). See options below.",
  								         #                         shiny::actionLink(inputId="occurrence_map_warning_got_it", label=strong("Got it !", style="color: #8d5524;"))),
  								         #     ),
  								         #
  								         #     shiny::div(id='occurrence_map_warning_options',
  								         #
  								         #                shiny::fluidRow(
  								         #
  								         #                  shiny::column(8,
  								         #
  								         #                                shiny::sliderInput(inputId = "occ_map_proportion",
  								         #
  								         #                                                   label= 'Proportion of records',
  								         #
  								         #                                                   min = 0,
  								         #
  								         #                                                   value = 10,
  								         #
  								         #                                                   max = 100,
  								         #
  								         #                                                   post = "%",
  								         #
  								         #                                                   step = 1)
  								         #                  ),
  								         #
  								         #                  shiny::column(4,
  								         #                                shiny::numericInput(inputId="rng", label="random number generator", value = 1234, min=1)
  								         #                  )
  								         #
  								         #                )
  								         #     )
  								         #   )
  								         # ),

  								         withSpinner(leafletOutput(outputId="myOccMap", width = "100%", height="700px"),type=5,size=1, color='#000000'),

  								         absolutePanel(id = "controls", class = "panel panel-default", fixed = TRUE,

  								                       draggable = TRUE, top = "50%", left = "auto", right = "1px", bottom = "auto",

  								                       width = "auto", height = "auto",

  								                       h3("Occurence Map",align='center'),

  								                       hr(),

  								                       p("Select your dataset"),

  								                       selectizeInput(inputId ="map_occ_dataset",

  								                                      label=NULL,

  								                                      choices=NULL,

  								                                      options=list(placeholder="Select an occurence dataset")
  								                       ),

  								                       br(),

  								                       hidden(

  								                         div(id="controls_species",

  								                             p("Select your species"),

  								                             uiOutput('chooser_map_occ_species_name'),

  								                             br()
  								                         ),

  								                         div(id="controls_attributes",

  								                             p("Select your attribute(s)"),

  								                             uiOutput('chooser_map_occ_attributes_name'),

  								                             br()
  								                         )
  								                       ),

  								                       selectizeInput(inputId="map_species_colors",

  								                                      label="Color Scheme",

  								                                      choices = rownames(subset(brewer.pal.info, category=='qual')),

  								                                      selected=NULL,

  								                                      options=list(placeholder='Select a color scheme')
  								                       ),

  								                       numericInput(inputId="map_circle_size",

  								                                    label="Circle size",

  								                                    min=1,

  								                                    value=5000
  								                       ),

  								                       sliderInput(inputId="map_circle_opacity",

  								                                   label="Opacity",

  								                                   min=0,

  								                                   max=1,

  								                                   step=0.01,

  								                                   value=0.7
  								                       ),

  								                       awesomeCheckbox(inputId="add_Legend",

  								                                       label="Show legend",

  								                                       value=TRUE,

  								                                       status = "primary"
  								                       ),

  								                       hidden(

  								                         div(id="add_myRegion_checkbox",

  								                             awesomeCheckbox(inputId="add_myRegion",

  								                                             label="Add polygon",

  								                                             value=FALSE,

  								                                             status = "primary"
  								                             )
  								                         )
  								                       ),

  								                       hr()
  								         )
  								  )
                  #)
               # )
							)
						)
			)
)
