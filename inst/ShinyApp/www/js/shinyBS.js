Shiny.addCustomMessageHandler("updateTooltipOrPopover", function(data) {
  if(data.action == "add") {
    shinyBS.addTooltip(data.id, data.type, data.options);
  } else if(data.action == "remove") {
    shinyBS.removeTooltip(data.id, data.type);
  }
});

var selectizeObserverDict = {};

Shiny.addCustomMessageHandler("updateSelectizeTooltipOrPopover", function(data) {

  var selectizeParent = document.getElementById(data.id).parentElement;

  if(data.action == "add") {
    var opts = $.extend(data.options, {html: true});

    var observer = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation){
        $(mutation.addedNodes).filter('div').filter(function(){return(this.getAttribute('data-value') == data.choice);}).each(function() {
          if(data.type == "tooltip"){
            $(this).tooltip('destroy');
            $(this).tooltip(opts);
          } else if (data.type == "popover") {
            $(this).popover('destroy');
            $(this).popover(opts);
          }
        });
      });
    });

    observer.observe(selectizeParent, { subtree: true, childList: true });

    selectizeObserverDict[data.type + data.id + data.choice] = observer;
  } else if(data.action == "remove") {

    selectizeObserverDict[data.type + data.id + data.choice].disconnect();

    $(selectizeParent).filter('div').filter(function(){return(this.getAttribute('data-value') == data.choice);}).each(function() {
      if(data.type == "tooltip"){
        $(this).tooltip('destroy');
      } else if (data.type == "popover") {
        $(this).popover('destroy');
      }
    });
  }
});

Shiny.addCustomMessageHandler("updateGroupInputTooltipOrPopover", function(data) {
  if(data.action == "add") {

    $("input", $("#" + data.id)).each(function() {
      if(this.getAttribute("value") == data.choice) {

        var opts = $.extend(data.options, {html: true});

        if(data.type == "tooltip") {
          $(this.parentElement).tooltip("destroy");
          $(this.parentElement).tooltip(opts);
        } else if(data.type == "popover") {
          $(this.parentElement).popover("destroy");
          $(this.parentElement).popover(opts);
        }
      }
    });

  } else if(data.action == "remove") {

    $("input", $("#" + data.id)).each(function() {
      if(this.getAttribute("value") == data.choice) {

        if(data.type == "tooltip") {
          $(this.parentElement).tooltip("destroy");
        } else if(data.type == "popover") {
          $(this.parentElement).popover("destroy");
        }
      }
    });
  }
});

Shiny.addCustomMessageHandler("bsButtonUpdate", function(data) {

  var btn = $("button#" + data.id);
  var ico = btn.find("i");

  if(ico.length > 0) {
    ico = ico[0].outerHTML;
  } else {
    ico = "";
  }

  if(data.hasOwnProperty("label")) {
    btn.html(ico + data.label);
  }

  if(data.hasOwnProperty("icon")) {
    var ch = btn.children();
    if(ch.length === 0) {
      btn.prepend(data.icon);
    } else {
      btn.find("i").replaceWith(data.icon);
    }
  }

  if(data.hasOwnProperty("value")) {
    if(btn.hasClass("sbs-toggle-button")) {
      if(data.value != btn.hasClass("active")) {
        btn.trigger("click");
      }
    }
  }

  if(data.hasOwnProperty("style")) {
    btn
    .removeClass("btn-default btn-primary btn-success btn-info btn-warning btn-danger btn-link")
    .addClass("btn-" + data.style);
  }

  if(data.hasOwnProperty("size")) {
    btn.removeClass("btn-lg btn-sm btn-xs");
    if(data.size != "default") {
      btn.addClass(data.size);
    }
  }

  if(data.hasOwnProperty("block")) {
    btn.toggleClass("btn-block", data.block);
  }

  if(data.hasOwnProperty("disabled")) {
    if(data.disabled) {
      btn.attr("disabled", "disabled");
    } else {
      btn.attr("disabled", false);
    }
  }

})
